package edu.info0502.joueur_poker;

/**
 * Représente une carte .
 * Une carte est définie par une valeur (numérique) et un signe (Couleur/Symbole).
 */
public class Carte implements Cloneable{
    private int valeur; // La valeur de la carte (1 pour As, 11 pour Valet, ...)
    private String signe; // Le signe ou symbole de la carte (Coeur, Carreau, Pique, Trèfle)

    /**
     * Constructeur par défaut.
     * Initialise une carte avec la valeur 3 et le signe "Coeur".
     */

    public Carte(){
        valeur = 3;
        signe = "Coeur";
    }

     /**
     * Constructeur avec paramètres.
     * Permet de définir une carte avec une valeur et un signe spécifiques.
     *
     * @param n La valeur de la carte (par exemple : 1 pour As, 11 pour Valet, etc.)
     * @param s Le signe de la carte (Coeur, Carreau, Pique, Trèfle)
     */
    public Carte(int n, String s){
        valeur = n;
        signe =s;
    }

    /**
     * Retourne la valeur de la carte.
     *
     * @return La valeur de la carte.
     */
    public int getVal(){
        return valeur;
    }
    /**
     * Retourne le signe de la carte.
     *
     * @return Le signe de la carte (Coeur, Carreau, Pique, Trèfle).
     */
    public String getSigne(){
        return signe;
    }

    /**
     * Crée une copie de cette carte.
     *
     * @return Une nouvelle instance de Carte avec les mêmes valeurs.
     * @throws CloneNotSupportedException Si l'objet ne peut pas être cloné.
     */
    @Override
    public Carte clone() throws CloneNotSupportedException {
        return (Carte) super.clone();
    }

     /**
     * Retourne une représentation textuelle de la carte.
     *
     * @return Une chaîne décrivant la carte (par exemple : "Carte 3 de Coeur").
     */
    @Override
    public String toString() {
        return "Carte " + valeur + " de " + signe;
    }

    
}


