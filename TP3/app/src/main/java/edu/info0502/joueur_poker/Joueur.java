package edu.info0502.joueur_poker;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Représente un joueur côté client dans une partie de poker.
 */
public class Joueur implements Serializable {
    private String nickname;
    private Carte[] cartes; // Les deux cartes privées du joueur

    /**
     * Constructeur pour initialiser un joueur.
     *
     * @param nickname Le pseudo du joueur.
     */
    public Joueur(String nickname) {
        this.nickname = nickname;
        this.cartes = new Carte[2]; // Le joueur commence avec 2 cartes nulles
    }

    /**
     * Retourne le pseudo du joueur.
     *
     * @return Le pseudo.
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * Définit les deux cartes privées du joueur.
     *
     * @param carte1 La première carte.
     * @param carte2 La deuxième carte.
     */
    public void setCartes(Carte carte1, Carte carte2) {
        this.cartes[0] = carte1;
        this.cartes[1] = carte2;
    }

    /**
     * Retourne les cartes privées du joueur.
     *
     * @return Un tableau contenant les deux cartes.
     */
    public Carte[] getCartes() {
        return cartes;
    }

    /**
     * Représente le joueur sous forme textuelle.
     *
     * @return Une chaîne décrivant le joueur et ses cartes.
     */
    @Override
    public String toString() {
        return "Joueur : " + nickname + "\nCartes : " + Arrays.toString(cartes);
    }
}
