package edu.info0502.joueur_poker;

import java.io.*;
import java.net.*;
import java.util.*;

/**
 * Client pour l'application de poker.
 * Cette classe gère la connexion au serveur, les interactions avec celui-ci, 
 * et l'affichage des messages reçus du serveur.
 */
public class PokerClient {
    private Socket socket; // Socket de connexion au serveur
    private ObjectOutputStream outputStream;// Flux de sortie pour envoyer des données au serveur
    private ObjectInputStream inputStream;// Flux d'entrée pour recevoir des données du serveur
    private String nickname; // Pseudonyme du client
    private Scanner scanner; // Instance unique du Scanner

     /**
     * Connecte le client au serveur.
     *
     * @param host L'adresse IP du serveur.
     * @param port Le port du serveur.
     */
    public void connect(String host, int port) {
        try {
            scanner = new Scanner(System.in); // Initialise Scanner une fois
            socket = new Socket(host, port);
            outputStream = new ObjectOutputStream(socket.getOutputStream());
            inputStream = new ObjectInputStream(socket.getInputStream());
            System.out.println("Connecté au serveur : " + host + ":" + port);

            // Choisir un pseudonyme
            chooseNickname(scanner); 

            // Démarrer l'écoute des messages du serveur
            new Thread(this::listenForMessages).start();

            // Interagir avec le serveur
            interactWithServer(scanner);

        } catch (IOException e) {
            System.err.println("Erreur lors de la connexion au serveur : " + e.getMessage());
        } finally {
            if (scanner != null) {
                scanner.close();
            }
        }
    }

    /**
     * Permet à l'utilisateur de choisir un pseudonyme.
     *
     * @param scanner Scanner pour lire la saisie utilisateur.
     * @throws IOException En cas d'erreur lors de l'envoi de données au serveur.
     */
    private void chooseNickname(Scanner scanner) throws IOException {
        while (nickname == null) {
            System.out.print("Entrez votre pseudo : ");
            if (scanner.hasNextLine()) {
                String tentative = scanner.nextLine();
                outputStream.writeObject(tentative);
                outputStream.flush();
    
                try {
                    Object response = inputStream.readObject();
                    if ("ACCEPTED".equals(response)) {
                        nickname = tentative;
                        System.out.println("Pseudo accepté : " + nickname);
                    } else {
                        System.out.println("Pseudo déjà pris. Essayez un autre.");
                    }
                } catch (ClassNotFoundException e) {
                    System.err.println("Erreur lors de la lecture de la réponse : " + e.getMessage());
                }
            } else {
                System.err.println("Aucune saisie détectée. Réessayez.");
            }
        }
    }
    

    /**
     * Écoute en continu les messages provenant du serveur.
     * Les messages reçus sont délégués à la méthode {@link #handleServerMessage(Object)}.
     */
    private void listenForMessages() {
        try {
            while (true) {
                Object message = inputStream.readObject();
                handleServerMessage(message);
            }
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("Connexion au serveur perdue.");
        }
    }

   /**
     * Gère les messages reçus du serveur.
     *
     * @param message Le message reçu (peut être un texte, une liste ou une carte).
     */
    private void handleServerMessage(Object message) {
        if (message instanceof String) {
            System.out.println("Message du serveur : " + message);
        } else if (message instanceof List) {
            @SuppressWarnings("unchecked")
            List<?> cartes = (List<?>) message;
            System.out.println("Cartes reçues : " + cartes);
        } else if (message instanceof Map) {
            @SuppressWarnings("unchecked")
            Map<String, ?> resultats = (Map<String, ?>) message;
            System.out.println("Résultats : ");
            resultats.forEach((joueur, score) ->
                System.out.println(joueur + " : " + score)
            );
        } else {
            System.out.println("Message non reconnu : " + message);
        }
    }


     /**
     * Gère les interactions avec le serveur via l'entrée utilisateur.
     *
     * @param scanner Scanner pour lire les commandes utilisateur.
     */
    private void interactWithServer(Scanner scanner) {
        while (true) {
            System.out.println("Options : START (lancer la partie), QUIT (quitter)");
            String command = scanner.nextLine().toUpperCase(); // Utilisation unique du Scanner

            try {
                outputStream.writeObject(command);
                outputStream.flush();
                if ("QUIT".equals(command)) {
                    System.out.println("Déconnexion...");
                    socket.close();
                    break;
                }
            } catch (IOException e) {
                System.err.println("Erreur lors de l'envoi de la commande : " + e.getMessage());
            }
        }
    }
    
    /**
     * Méthode principale pour démarrer le client.
     *
     * @param args Arguments de la ligne de commande (non utilisés ici).
     */
    public static void main(String[] args) {
        PokerClient client = new PokerClient();
        client.connect("10.11.17.215", 12345); 
    }
}
