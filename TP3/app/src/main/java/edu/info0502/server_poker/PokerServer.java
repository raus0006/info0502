package edu.info0502.server_poker;

import java.io.*;
import java.net.*;
import java.util.*;

/**
 * Serveur pour une partie de poker.
 * Ce serveur gère la connexion des joueurs, la gestion des parties, et les interactions client-serveur.
 */
public class PokerServer {
    private int port;// Port sur lequel le serveur écoute
    private List<ClientHandler> clients; // Liste des clients connectés
    private boolean gameInProgress = false;// Indique si une partie est en cours

    /**
     * Constructeur du serveur.
     *
     * @param port Le port sur lequel le serveur écoute.
     */
    public PokerServer(int port) {
        this.port = port;
        this.clients = new ArrayList<>();
    }

    /**
     * Démarre le serveur et accepte les connexions clients.
     *
     * @throws IOException Si une erreur survient lors de la création du socket serveur.
     */
    public void startServer() throws IOException {
        ServerSocket serverSocket = new ServerSocket(port);
        System.out.println("Serveur démarré sur le port " + port);

        while (true) {
            Socket clientSocket = serverSocket.accept();
            System.out.println("Nouvelle connexion !");
            ClientHandler clientHandler = new ClientHandler(clientSocket, this);
            clients.add(clientHandler);
            new Thread(clientHandler).start();
        }
    }

    /**
     * Vérifie si une partie est en cours.
     *
     * @return {@code true} si une partie est en cours, {@code false} sinon.
     */
    public synchronized boolean isGameInProgress() {
        return gameInProgress;
    }


    /**
     * Démarre une nouvelle partie de poker.
     * Cette méthode gère la distribution des cartes, le flop, le turn, la river, et détermine le vainqueur.
     */
    public synchronized void startGame() {
        if (clients.size() < 2) {
            try {
                broadcast("Pas assez de joueurs pour commencer.");
            } catch (IOException e) {
                System.err.println("Erreur lors de la diffusion : " + e.getMessage());
            }
            return;
        }
    
        try {
            broadcast("La partie commence !");
        } catch (IOException e) {
            System.err.println("Erreur lors de la diffusion : " + e.getMessage());
        }
        gameInProgress = true;
    
        // Création du talon et distribution des cartes
        Talon talon = new Talon();
        for (ClientHandler client : clients) {
            List<Carte> cartesCachees = Arrays.asList(talon.tirerCarte(), talon.tirerCarte());
            client.setCartesCachees(cartesCachees);
            try {
                client.sendMessage("Vos cartes : " + cartesCachees);
            } catch (IOException e) {
                System.err.println("Erreur d'envoi des cartes au client " + client.getNickname());
            }
        }
    
        // Board (flop, turn, river)
        List<Carte> board = new ArrayList<>();
        try {
            board.addAll(Arrays.asList(talon.tirerCarte(), talon.tirerCarte(), talon.tirerCarte())); // Flop
            broadcast("Flop : " + board);
            board.add(talon.tirerCarte()); // Turn
            broadcast("Turn : " + board);
            board.add(talon.tirerCarte()); // River
            broadcast("River : " + board);
        } catch (IOException e) {
            System.err.println("Erreur lors de la diffusion : " + e.getMessage());
        }
    
        // Évaluation des mains
        Map<String, Integer> scores = new HashMap<>();
        Map<String, String> combinaisons = new HashMap<>();
        int meilleurScore = 0;
        String vainqueur = null;
    
        for (ClientHandler client : clients) {
            List<Carte> mainComplete = new ArrayList<>(client.getCartesCachees());
            mainComplete.addAll(board);
            Mains meilleureMain = Mains.calculerMeilleureMain(mainComplete);
            int score = meilleureMain.evaluer();
            scores.put(client.getNickname(), score);
            combinaisons.put(client.getNickname(), meilleureMain.afficherCombinaison());
    
            if (score > meilleurScore) {
                meilleurScore = score;
                vainqueur = client.getNickname();
            }
        }
    
        try {
            broadcast("Résultats des joueurs : " + combinaisons);
            broadcast("Le gagnant est " + vainqueur + " avec " + combinaisons.get(vainqueur) + "!");
        } catch (IOException e) {
            System.err.println("Erreur lors de la diffusion des résultats : " + e.getMessage());
        }
    
        gameInProgress = false;
    }
    
     /**
     * Diffuse un message à tous les clients connectés.
     *
     * @param message Le message à diffuser.
     * @throws IOException Si une erreur survient lors de l'envoi.
     */
    public synchronized void broadcast(Object message) throws IOException {
        for (ClientHandler client : clients) {
            client.sendMessage(message);
        }
    }

    /**
     * Supprime un client de la liste des clients connectés.
     *
     * @param client Le client à supprimer.
     */
    public synchronized void removeClient(ClientHandler client) {
        clients.remove(client);
    }

    /**
     * Vérifie si un pseudonyme est déjà utilisé.
     *
     * @param nickname Le pseudonyme à vérifier.
     * @return {@code true} si le pseudonyme est déjà utilisé, {@code false} sinon.
     */
    public synchronized boolean isNicknameTaken(String nickname) {
        for (ClientHandler client : clients) {
            if (client.getNickname() != null && client.getNickname().equalsIgnoreCase(nickname)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Point d'entrée principal pour démarrer le serveur.
     *
     * @param args Arguments de la ligne de commande.
     */
    public static void main(String[] args) {
        try {
            PokerServer server = new PokerServer(12345);
            server.startServer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
