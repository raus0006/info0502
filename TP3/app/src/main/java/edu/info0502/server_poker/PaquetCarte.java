package edu.info0502.server_poker;



import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Représente un paquet de cartes.
 * Le paquet peut contenir un nombre défini de cartes et propose des méthodes
 * pour mélanger et tirer des cartes.
 */
public class PaquetCarte implements Cloneable{

    private int nbCartes;  // Nombre total de cartes dans le paquet
    private List<Carte> paquet;// Liste des cartes dans le paquet

     /**
     * Constructeur par défaut.
     * Initialise un paquet contenant 10 cartes avec des valeurs de 1 à 13
     * réparties sur les quatre signes (Coeur, Carreau, Trèfle, Pique).
     */
    public PaquetCarte(){
        paquet = new ArrayList<>();
        nbCartes = 10;

        String[] signes = {"Coeur", "Carreau", "Trefle", "Pique"};
        int valeur = 1;

        for (int i = 0; i < nbCartes; i++) {
           paquet.add(new Carte(valeur, signes[i % signes.length]));
            valeur++;
            if (valeur > 13) {
                valeur = 1; // Renouvelle les valeurs après 13
            }
        }
    }
    /**
     * Constructeur avec paramètres.
     * Permet de créer un paquet avec un nombre défini de cartes et une liste donnée.
     *
     * @param n  Nombre de cartes dans le paquet.
     * @param pc Liste des cartes à inclure dans le paquet.
     */
    public PaquetCarte(int n, List<Carte> pc){
        nbCartes = n;
        paquet = pc;
    }

     /**
     * Crée une copie du paquet de cartes.
     *
     * @return Une nouvelle instance de PaquetCarte identique à l'original.
     * @throws CloneNotSupportedException Si le clonage n'est pas supporté.
     */
    @Override
    public PaquetCarte clone() throws CloneNotSupportedException{
        PaquetCarte clone = (PaquetCarte)super.clone();
        clone.paquet = new ArrayList<>(this.paquet);
        return clone;
    }
    /**
     * Retourne le nombre de cartes dans le paquet.
     *
     * @return Le nombre total de cartes.
     */
    public int getNbCartes(){
        return nbCartes;
    }
    /**
     * Retourne la liste des cartes dans le paquet.
     *
     * @return Une liste contenant les cartes du paquet.
     */
    public List<Carte> getCartes(){
        return new ArrayList<>(paquet);
    }

    /**
     * Tire une carte du paquet.
     * La carte est retirée de la liste des cartes disponibles.
     *
     * @return La carte tirée, ou {@code null} si le paquet est vide.
     */
    public Carte tirerCarte() {
        if (paquet.isEmpty()) {
            return null;  
        } else {
            return paquet.remove(0);  
        }
    }
     /**
     * Mélange les cartes du paquet.
     */
    public void melangerCartes(){
        Collections.shuffle(paquet);
    }
    /**
     * Retourne une représentation textuelle du paquet.
     *
     * @return Une chaîne décrivant toutes les cartes du paquet.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Paquet de carte :\n");
        for (Carte m : paquet) {
            sb.append(m.toString()).append("\n");
        }
        return sb.toString();
    }

}
