package edu.info0502.server_poker;

import java.io.*;
import java.net.*;
import java.util.*;


/**
 * Gestionnaire de client pour le serveur de poker.
 * Cette classe gère les interactions entre le serveur et un client individuel.
 */
public class ClientHandler implements Runnable {
    private final Socket socket;  // Socket de communication avec le client
    private final PokerServer server; // Référence au serveur
    private ObjectOutputStream outputStream; // Flux de sortie pour envoyer des données au client
    private ObjectInputStream inputStream; // Flux d'entrée pour recevoir des données du client
    private String nickname; // Pseudo du client
    private List<Carte> cartesCachees = new ArrayList<>(); // Cartes privées du client


    /**
     * Constructeur de la classe ClientHandler.
     *
     * @param clientSocket Le socket associé au client.
     * @param server       Le serveur de poker.
     */
    public ClientHandler(Socket clientSocket, PokerServer server) {
        this.socket = clientSocket;
        this.server = server;
        try {
            outputStream = new ObjectOutputStream(clientSocket.getOutputStream());
            outputStream.flush();
            inputStream = new ObjectInputStream(clientSocket.getInputStream());
            System.out.println("Client connecté : " + clientSocket.getInetAddress());
        } catch (IOException e) {
            System.err.println("Erreur lors de la connexion au client : " + e.getMessage());
        }
    }
    /**
     * Méthode principale exécutée dans le thread du client.
     * Gère l'interaction avec le client : choix du pseudo, traitement des commandes, déconnexion.
     */
    @Override
    public void run() {
        try {
            // Demander un pseudo
            while (nickname == null) {
                sendMessage("Entrez un pseudo :");
                String tentative = (String) inputStream.readObject();
                System.out.println("Pseudo proposé : " + tentative);
            
                if (!server.isNicknameTaken(tentative)) {
                    nickname = tentative;
                    sendMessage("ACCEPTED");
                } else {
                    sendMessage("Nickname déjà pris. Essayez un autre.");
                }
            }
            System.out.println(nickname + " connecté.");            
            server.broadcast(nickname + " a rejoint la partie.");

            // Participer à la partie
            while (true) {
                Object message = inputStream.readObject();
                if (message instanceof String) {
                    String command = ((String) message).toUpperCase();
                    switch (command) {
                        case "START":
                            if (!server.isGameInProgress()) {
                                server.startGame();
                            } else {
                                sendMessage("Partie en cours. Veuillez attendre.");
                            }
                            break;
                        case "QUIT":
                            server.removeClient(this);
                            sendMessage("Déconnecté.");
                            socket.close();
                            return;
                        default:
                            sendMessage("Commande non reconnue : " + command);
                            break;
                    }
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("Erreur avec le client " + nickname + ": " + e.getMessage());
        } finally {
            try {
                server.removeClient(this);
                socket.close();
            } catch (IOException ex) {
                System.err.println("Erreur lors de la fermeture du socket : " + ex.getMessage());
            }
            System.out.println("Client " + nickname + " déconnecté.");
        }
    }

     /**
     * Envoie un message au client.
     *
     * @param message Le message à envoyer.
     * @throws IOException Si une erreur survient lors de l'envoi.
     */
    public void sendMessage(Object message) throws IOException {
        outputStream.writeObject(message);
        outputStream.flush();
    }

    /**
     * Retourne le pseudonyme du client.
     *
     * @return Le pseudonyme du client.
     */
    public String getNickname() {
        return nickname;
    }

     /**
     * Définit les cartes privées du client.
     *
     * @param cartesCachees La liste des cartes privées.
     */
    public void setCartesCachees(List<Carte> cartesCachees) {
        this.cartesCachees = cartesCachees;
    }
    /**
     * Retourne les cartes privées du client.
     *
     * @return Une liste des cartes privées.
     */
    public List<Carte> getCartesCachees() {
        return cartesCachees;
    }
}
