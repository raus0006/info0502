package edu.info0502.jeudecarte;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Représente une main de poker composée de 5 cartes.
 * Une main est générée à partir d'un talon ou peut être définie manuellement.
 */
public class Mains implements Cloneable{

    private Carte[] main;

    /**
     * Constructeur par défaut.
     * Génère une main de 5 cartes à partir d'un talon initialisé avec 3 paquets.
     * Les cartes sont distribuées en utilisant un index basé sur le reste des divisions.
     */
    public Mains() {
        Talon t = new Talon();
        main = new Carte[5];
    
        List<PaquetCarte> paquets = t.getPaquetC();
        if (paquets == null || paquets.isEmpty()) {
            throw new IllegalStateException("Talon invalide : aucun paquet disponible.");
        }
    
        for (int i = 0; i < 5; i++) {
            PaquetCarte paquet = paquets.get(i % paquets.size());
            Carte carte = paquet.tirerCarte();
            if (carte == null) {
                throw new IllegalStateException("Talon invalide : pas assez de cartes.");
            }
            main[i] = carte;
        }
    }
    

    /**
     * Constructeur avec paramètres.
     * Permet de définir une main avec un tableau spécifique de cartes.
     *
     * @param cartes Tableau contenant les 5 cartes de la main.
     */
    public Mains(Carte[] cartes){
        if (cartes.length <= 5) {
            main = cartes.clone();
        }
    }

    /**
     * Retourne les cartes de la main.
     *
     * @return Un tableau contenant les cartes de la main.
     */
    public Carte[] getMain(){
        return main.clone();
    }

    /**
     * Évalue la main pour déterminer la combinaison la plus forte.
     * Les combinaisons possibles incluent (du plus faible au plus fort) :
     * - Paire
     * - Double paire
     * - Brelan
     * - Suite
     * - Flush
     * - Full
     * - Carré
     * - Quinte flush
     *
     * @return Un score numérique représentant la force de la combinaison.
     *         Plus le score est élevé, plus la combinaison est forte.
     */
    public int evaluer() {

        if (main == null || main.length == 0) {
            System.err.println("Erreur : La main est vide ou null.");
            return -1;
        }
    
        for (Carte carte : main) {
            if (carte == null) {
                System.err.println("Erreur : La main contient une carte null.");
                return -1;
            }
        }
        Map<Integer, Integer> valeurCount = new HashMap<>();
        Map<String, Integer> couleurCount = new HashMap<>();

        // Compter les occurrences des valeurs et des couleurs
        for (Carte carte : main) {
            valeurCount.put(carte.getVal(), valeurCount.getOrDefault(carte.getVal(), 0) + 1);
            couleurCount.put(carte.getSigne(), couleurCount.getOrDefault(carte.getSigne(), 0) + 1);
        }

        boolean isFlush = couleurCount.containsValue(5); // Toutes les cartes ont la même couleur
        boolean isStraight = isStraight(valeurCount.keySet()); // Consécutives en valeur

        // Quinte flush
        if (isFlush && isStraight) return 8;

        // Carré
        if (valeurCount.containsValue(4)) return 7;

        // Full
        if (valeurCount.containsValue(3) && valeurCount.containsValue(2)) return 6;

        // Flush
        if (isFlush) return 5;

        // Suite
        if (isStraight) return 4;

        // Brelan
        if (valeurCount.containsValue(3)) return 3;

        // Double paire
        if (valeurCount.values().stream().filter(count -> count == 2).count() == 2) return 2;

        // Paire
        if (valeurCount.containsValue(2)) return 1;

        // Carte haute
        return 0;
    }

    /**
     * Vérifie si les cartes forment une suite (valeurs consécutives).
     *
     * @param valeurs Un ensemble des valeurs uniques des cartes de la main.
     * @return {@code true} si les cartes forment une suite, {@code false} sinon.
     */
    private boolean isStraight(Set<Integer> valeurs) {
        List<Integer> sorted = new ArrayList<>(valeurs);
        Collections.sort(sorted);

        // Vérifie si les valeurs sont consécutives
        for (int i = 0; i < sorted.size() - 1; i++) {
            if (sorted.get(i + 1) - sorted.get(i) != 1) {
                return false;
            }
        }

        // Cas particulier : suite "As-2-3-4-5"
        return sorted.contains(1) && sorted.contains(10) && sorted.contains(11) &&
            sorted.contains(12) && sorted.contains(13);
    }

    /**
    * Retourne une description textuelle de la combinaison détectée dans la main.
    *
    * @return Une chaîne décrivant la combinaison (par exemple : "Paire", "Brelan", etc.).
    */
    public String afficherCombinaison() {
        int score = evaluer();
        switch (score) {
            case 8: return "Quinte Flush";
            case 7: return "Carré";
            case 6: return "Full";
            case 5: return "Flush";
            case 4: return "Suite";
            case 3: return "Brelan";
            case 2: return "Double Paire";
            case 1: return "Paire";
            default: return "Carte Haute";
        }
    }

    /**
     * Calcule la meilleure main possible à partir d'une liste de cartes.
     * Cette méthode considère toutes les combinaisons possibles de 5 cartes parmi les cartes fournies.
     *
     * @param mainComplete Une liste de 7 cartes (2 cartes privées + 5 cartes communes).
     * @return La meilleure main calculée (instance de Mains).
     */
    public static Mains calculerMeilleureMain(List<Carte> mainComplete) {
        if (mainComplete.size() != 7) {
            throw new IllegalArgumentException("La liste doit contenir exactement 7 cartes.");
        }

        Mains meilleureMain = null;
        int meilleurScore = -1;

        // Générer toutes les combinaisons de 5 cartes parmi les 7 cartes
        List<List<Carte>> combinaisons = combinaisonsDe5(mainComplete);

        // Évaluer chaque combinaison et garder la meilleure
        for (List<Carte> combinaison : combinaisons) {
            Mains main = new Mains(combinaison.toArray(new Carte[0]));
            int score = main.evaluer();

            if (score > meilleurScore) {
                meilleurScore = score;
                meilleureMain = main;
            }
        }

        return meilleureMain;
    }

    /**
     * Génère toutes les combinaisons possibles de 5 cartes parmi une liste donnée.
     *
     * @param cartes Une liste de cartes (par exemple, 7 cartes).
     * @return Une liste contenant toutes les combinaisons de 5 cartes.
     */
    private static List<List<Carte>> combinaisonsDe5(List<Carte> cartes) {
        List<List<Carte>> combinaisons = new ArrayList<>();
        combinaisonRecursive(cartes, 5, 0, new ArrayList<>(), combinaisons);
        return combinaisons;
    }

    /**
     * Méthode récursive pour générer les combinaisons de cartes.
     *
     * @param cartes         Liste originale des cartes.
     * @param tailleCombinaison Taille souhaitée des combinaisons.
     * @param index          Index actuel dans la liste.
     * @param combinaisonActuelle Combinaison temporaire en cours de construction.
     * @param combinaisons   Liste finale contenant toutes les combinaisons générées.
     */
    private static void combinaisonRecursive(List<Carte> cartes, int tailleCombinaison, int index, List<Carte> combinaisonActuelle, List<List<Carte>> combinaisons) {
        if (combinaisonActuelle.size() == tailleCombinaison) {
            combinaisons.add(new ArrayList<>(combinaisonActuelle));
            return;
        }

        if (index >= cartes.size()) {
            return;
        }

        // Inclure la carte actuelle dans la combinaison
        combinaisonActuelle.add(cartes.get(index));
        combinaisonRecursive(cartes, tailleCombinaison, index + 1, combinaisonActuelle, combinaisons);

        // Ne pas inclure la carte actuelle
        combinaisonActuelle.remove(combinaisonActuelle.size() - 1);
        combinaisonRecursive(cartes, tailleCombinaison, index + 1, combinaisonActuelle, combinaisons);
    }


    

    /**
     * Compare cette main avec une autre main.
     *
     * @param autreMain La main à comparer.
     * @return Un entier indiquant le résultat de la comparaison :
     *         - Valeur négative si cette main est plus faible.
     *         - 0 si les deux mains sont égales.
     *         - Valeur positive si cette main est plus forte.
     */
    public int comparer(Mains autreMain) {
        return Integer.compare(this.evaluer(), autreMain.evaluer());
    }


    /**
     * Crée une copie de cette main.
     *
     * @return Une nouvelle instance de Mains identique à l'originale.
     * @throws CloneNotSupportedException Si le clonage n'est pas supporté.
     */
    @Override
    public Mains clone() throws CloneNotSupportedException {
        Mains clone = (Mains) super.clone();
        clone.main = main.clone(); 
        return clone;
    }

     /**
     * Retourne une représentation textuelle de la main.
     *
     * @return Une chaîne décrivant les cartes de la main.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Main :\n");
        for (Carte carte : main) {
            sb.append(carte.toString()).append("\n");
        }
        return sb.toString();
    }

   
    

}
