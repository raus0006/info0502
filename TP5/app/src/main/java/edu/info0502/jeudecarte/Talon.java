package edu.info0502.jeudecarte;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Représente un talon de cartes composé de plusieurs paquets de cartes.
 * Le talon permet de tirer des cartes aléatoirement et de mélanger les paquets.
 */

public class Talon implements Cloneable{

    int nbPaquets;// Nombre de paquets dans le talon
    private List<PaquetCarte> pcartes;// Liste des paquets de cartes

    /**
     * Constructeur par défaut.
     * Initialise un talon avec 3 paquets de cartes mélangés.
     */
    public Talon(){
        pcartes = new ArrayList<>();
        nbPaquets = 3;
        for(int i=0; i<nbPaquets; i++){
            PaquetCarte temp = new PaquetCarte();
            temp.melangerCartes(); 
            pcartes.add(temp);
        }
    }

    /**
     * Constructeur avec paramètres.
     * Permet de définir un talon contenant un nombre spécifique de paquets.
     *
     * @param n  Le nombre de paquets dans le talon.
     * @param pc La liste des paquets à inclure dans le talon.
     */
    public Talon(int n, List<PaquetCarte> pc){
        nbPaquets = n;
        pcartes =  new ArrayList<>(pc);
    }

     /**
     * Crée une copie du talon.
     *
     * @return Une nouvelle instance de Talon identique à l'original.
     * @throws CloneNotSupportedException Si le clonage n'est pas supporté.
     */
    @Override
    public Talon clone() throws CloneNotSupportedException{
        Talon clone = (Talon)super.clone();
        clone.pcartes = new ArrayList<>(this.pcartes);
        return clone;
    }
    /**
     * Retourne le nombre de paquets dans le talon.
     *
     * @return Le nombre total de paquets.
     */
    public int getNbPaquets(){
        return nbPaquets;
    }
    /**
     * Retourne la liste des paquets de cartes.
     *
     * @return Une liste contenant les paquets de cartes.
     */
    public List<PaquetCarte> getPaquetC(){
        return new ArrayList<>(pcartes); 
    }

    /**
     * Affiche toutes les cartes présentes dans le talon.
     * Parcourt chaque paquet de cartes et affiche leur contenu.
     */
    public void afficherToutesLesCartes() {
        for (PaquetCarte paquet : pcartes) {
            System.out.println(paquet);
        }
    }

    /**
     * Retourne le nombre total de cartes présentes dans le talon.
     *
     * @return Le nombre total de cartes.
     */
    public int getTaille() {
        int total = 0;
        for (PaquetCarte paquet : pcartes) {
            total += paquet.getNbCartes();
        }
        return total;
    }

    /**
     * Recharge le talon avec un nombre spécifique de paquets.
     *
     * @param nombrePaquets Le nombre de paquets à ajouter dans le talon.
     */
    public void remplirTalon(int nombrePaquets) {
        for (int i = 0; i < nombrePaquets; i++) {
            PaquetCarte nouveauPaquet = new PaquetCarte();
            nouveauPaquet.melangerCartes();
            pcartes.add(nouveauPaquet);
        }
    }

    /**
    * Affiche le nombre total de cartes restantes dans le talon.
    */
    public void afficherNbCartesRestantes() {
        System.out.println("Cartes restantes dans le talon : " + getTaille());
    } 

    /**
     * Mélange les paquets de cartes dans le talon.
     */
    public void melanger() {
        Collections.shuffle(pcartes);
    }

    /**
     * Retourne une représentation textuelle du talon.
     *
     * @return Une chaîne décrivant tous les paquets du talon.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Talon de cartes :\n");
        for (PaquetCarte paquet : pcartes) {
            sb.append(paquet.toString()).append("\n");
        }
        return sb.toString();
    }

    /**
     * Tire une carte aléatoirement d'un des paquets du talon.
     * Si le paquet est vide, une autre tentative est effectuée.
     *
     * @return La carte tirée, ou {@code null} si le talon est vide.
     */
    public Carte tirerCarte() {
        if (pcartes.isEmpty()) {
            return null; 
        }
        Random randomNb = new Random();
        int indice = randomNb.nextInt(nbPaquets); // Indice aléatoire
        PaquetCarte paquet = pcartes.get(indice);

        // Retire une carte du paquet sélectionné
        if (!paquet.getCartes().isEmpty()) {
            return paquet.tirerCarte();
        } else {
            // Retente de tirer une carte si le paquet est vide
            for (PaquetCarte p : pcartes) {
                if (!p.getCartes().isEmpty()) {
                    return p.tirerCarte();
                }
            }
            return null; // Tous les paquets sont vides
        }
    }

}

