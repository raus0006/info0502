package edu.info0502.serveur;

import org.eclipse.paho.client.mqttv3.*;
import java.util.*;

import edu.info0502.jeudecarte.Carte;
import edu.info0502.jeudecarte.Mains;
import edu.info0502.jeudecarte.Talon;

/**
 * Classe principale du serveur de poker.
 * Gère les joueurs, les parties, les cartes et les résultats.
 */
public class PokerServer {
    private final String brokerUrl = "tcp://localhost:1883";
    private final String gameTopic = "poker/game";
    private final MqttClient mqttClient;
    private boolean gameInProgress = false;
    private volatile boolean startCommandReceived = false;

    private final List<String> players = new ArrayList<>();
    private final Map<String, List<Carte>> playerCards = new HashMap<>();
    private final List<Carte> boardCards = new ArrayList<>();

    /**
     * Constructeur qui initialise la connexion MQTT et configure les callbacks.
     *
     * @throws MqttException en cas de problème de connexion au broker MQTT.
     */
    public PokerServer() throws MqttException {
        mqttClient = new MqttClient(brokerUrl, MqttClient.generateClientId());
        mqttClient.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable cause) {
                System.err.println("Connexion perdue : " + cause.getMessage());
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                handleIncomingMessage(topic, new String(message.getPayload()));
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {
                // Rien à gérer
            }
        });
        mqttClient.connect();
        mqttClient.subscribe(gameTopic);
    }

    /**
     * Démarre le serveur MQTT et initialise les abonnements.
     */
    public void startServer() {
        try {
            if (!mqttClient.isConnected()) {
                mqttClient.connect();
            }
            mqttClient.subscribe(gameTopic);
            System.out.println("Serveur MQTT démarré et prêt !");
        } catch (MqttException e) {
            System.err.println("Erreur lors du démarrage du serveur : " + e.getMessage());
        }
    }

    /**
     * Arrête le serveur MQTT proprement.
     */
    public void stopServer() {
        try {
            if (mqttClient.isConnected()) {
                mqttClient.disconnect();
                System.out.println("Serveur MQTT arrêté.");
            }
        } catch (MqttException e) {
            System.err.println("Erreur lors de l'arrêt du serveur : " + e.getMessage());
        }
    }

    /**
     * Boucle principale pour gérer les parties.
     * Une nouvelle manche est lancée si deux joueurs ou plus sont connectés et que la commande START est reçue.
     */
    public void startGameLoop() {
        while (true) {
            try {
                if (players.size() >= 2 && startCommandReceived) {
                    System.out.println("Nouvelle manche démarrée !");
                    startCommandReceived = false;
                    startGame();
                }
                Thread.sleep(500); // Pause pour éviter une boucle agressive
            } catch (InterruptedException e) {
                System.err.println("Erreur dans la boucle de jeu : " + e.getMessage());
            }
        }
    }

    /**
     * Gère les messages reçus via MQTT.
     *
     * @param topic   Le sujet du message.
     * @param message Le contenu du message.
     */
    private void handleIncomingMessage(String topic, String message) {
        if (topic.equals(gameTopic)) {
            if (message.startsWith("JOIN:")) {
                String playerName = message.split(":")[1];
                if (!players.contains(playerName)) {
                    players.add(playerName);
                    System.out.println(playerName + " a rejoint la partie.");
                }
            } else if (message.equals("START")) {
                if (players.size() >= 2 && !gameInProgress) {
                    System.out.println("Commande START reçue.");
                    startCommandReceived = true;
                } else {
                    System.out.println("Impossible de démarrer : pas assez de joueurs ou partie en cours.");
                }
            }
        }
    }

    /**
     * Lance une nouvelle manche.
     */
    private void startGame() {
        gameInProgress = true;
        System.out.println("La partie commence !");
        distributeCards();
        sendBoardCards();
        calculateResults();
        gameInProgress = false;
    }

    /**
     * Distribue deux cartes à chaque joueur.
     */
    private void distributeCards() {
        Talon talon = new Talon();

        for (String player : players) {
            List<Carte> cards = Arrays.asList(talon.tirerCarte(), talon.tirerCarte());
            playerCards.put(player, cards);
            sendMessage("Vos cartes : " + cards, player);
        }
    }

    /**
     * Tire cinq cartes pour les cartes communes et les diffuse à tous les joueurs.
     */
    private void sendBoardCards() {
        Talon talon = new Talon();
        boardCards.clear();

        while (boardCards.size() < 5) {
            Carte carte = talon.tirerCarte();
            if (carte != null) {
                boardCards.add(carte);
            } else {
                System.err.println("Erreur : Une carte tirée pour le board est nulle.");
            }
        }

        sendMessage("Cartes communes : " + boardCards, null);
    }

    /**
     * Calcule les résultats pour chaque joueur et les envoie .
     */
    private void calculateResults() {
        Map<String, Integer> scores = new HashMap<>();
        Map<String, String> combinations = new HashMap<>();
    
        for (String player : players) {
            // Obtenir les cartes privées du joueur
            List<Carte> playerHand = playerCards.get(player);
    
            // Ajouter les cartes communes
            List<Carte> completeHand = new ArrayList<>(playerHand);
            completeHand.addAll(boardCards);
    
            // Calculer le score de la main avec Mains
            Mains mains = Mains.calculerMeilleureMain(completeHand);
            int score = mains.evaluer();
            String combination = mains.afficherCombinaison();
    
            scores.put(player, score);
            combinations.put(player, combination);
        }
    
        // Construire un message de résultats globaux
        StringBuilder globalResults = new StringBuilder("Résultats globaux :\n");
        for (String player : players) {
            globalResults.append(player)
                         .append(" : ")
                         .append(combinations.get(player))
                         .append(" (Score : ")
                         .append(scores.get(player))
                         .append(")\n");
        }
    
        // Diffuser les résultats globaux à tous les joueurs
        sendMessage(globalResults.toString(), null);
    
        // Log des scores côté serveur pour debug
        System.out.println(globalResults.toString());
    }
    

    /**
     * Envoie un message via MQTT.
     *
     * @param message Le contenu du message.
     * @param player  Le joueur cible (ou null pour un message global).
     */
    private void sendMessage(String message, String player) {
        try {
            if (player == null) {
                mqttClient.publish(gameTopic, new MqttMessage(message.getBytes()));
            } else {
                mqttClient.publish("poker/player/" + player, new MqttMessage(message.getBytes()));
            }
        } catch (MqttException e) {
            System.err.println("Erreur lors de l'envoi du message : " + e.getMessage());
        }
    }
}
