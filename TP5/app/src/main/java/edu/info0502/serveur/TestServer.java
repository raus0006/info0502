package edu.info0502.serveur;

import org.eclipse.paho.client.mqttv3.MqttException;

/**
 * Classe principale pour démarrer le serveur PokerServer.
 * Elle initialise le serveur MQTT et démarre la boucle principale du jeu.
 */
public class TestServer {
    public static void main(String[] args) {
        try {
            // Initialisation du serveur PokerServer
            PokerServer server = new PokerServer();

            // Démarrage du serveur MQTT
            server.startServer();

            // Démarrage de la boucle principale du jeu
            server.startGameLoop();

        } catch (MqttException e) {
            // Gestion des erreurs liées au serveur MQTT
            System.err.println("Erreur lors du démarrage du serveur : " + e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            // Gestion des autres exceptions
            System.err.println("Une erreur inattendue s'est produite : " + e.getMessage());
            e.printStackTrace();
        }
    }
}
