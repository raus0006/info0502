package edu.info0502.serveur;

import java.util.Properties;

/**
 * Classe de configuration du serveur.
 * Cette classe centralise les paramètres du serveur MQTT
 * pour éviter de dupliquer les chaînes de connexion dans le code.
 */
public class ServerConfig {
    // Propriétés contenant les paramètres du serveur
    private final Properties properties = new Properties();

    /**
     * Constructeur par défaut.
     * Initialise les paramètres de configuration par défaut, 
     * incluant l'URL du broker et les sujets utilisés.
     */
    public ServerConfig() {
        properties.setProperty("broker.url", "tcp://localhost:1883"); // Adresse par défaut du broker MQTT
        properties.setProperty("game.topic", "poker/game");          // Sujet pour les messages de jeu
        properties.setProperty("result.topic", "poker/result");      // Sujet pour les résultats
    }

    /**
     * Récupère une propriété spécifique.
     *
     * @param key La clé de la propriété (par exemple : "broker.url").
     * @return La valeur associée à la clé, ou {@code null} si elle n'existe pas.
     */
    public String getProperty(String key) {
        return properties.getProperty(key);
    }
}
