package edu.info0502.client;

import java.util.Scanner;

/**
 * Classe principale pour exécuter le client PokerClient.
 * Elle permet de connecter un utilisateur au serveur de jeu.
 */
public class TestClient {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String nickname;

        try {
            // Vérifie si un pseudo est fourni en argument, sinon demande à l'utilisateur
            if (args.length > 0) {
                nickname = args[0];
            } else {
                System.out.print("Entrez votre pseudo : ");
                nickname = scanner.hasNextLine() ? scanner.nextLine() : "JoueurParDefaut";
            }

            // Initialisation et interaction avec le serveur
            PokerClient client = new PokerClient(nickname);
            client.interactWithServer();

        } catch (Exception e) {
            // Gestion des erreurs générales
            System.err.println("Erreur lors de l'exécution du client : " + e.getMessage());
            e.printStackTrace();
        } finally {
            // Fermeture du scanner pour libérer les ressources
            scanner.close();
        }
    }
}
