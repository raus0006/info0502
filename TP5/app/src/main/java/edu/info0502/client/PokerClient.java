package edu.info0502.client;

import org.eclipse.paho.client.mqttv3.*;
import java.util.Scanner;

/**
 * Classe représentant un client de poker.
 * Permet aux joueurs de se connecter au serveur, d'interagir avec lui et de recevoir des messages.
 */
public class PokerClient {
    private final String brokerUrl = "tcp://localhost:1883"; // URL du broker MQTT
    private final String gameTopic = "poker/game";           // Sujet pour les messages généraux
    private final String playerTopicPrefix = "poker/player/"; // Préfixe pour les messages privés
    private MqttClient mqttClient;
    private String nickname;

    /**
     * Constructeur.
     * Initialise le client avec le pseudo fourni et configure la connexion MQTT.
     *
     * @param nickname Le pseudo du joueur.
     */
    public PokerClient(String nickname) {
        this.nickname = nickname;
        try {
            mqttClient = new MqttClient(brokerUrl, MqttClient.generateClientId());
            mqttClient.setCallback(new MqttCallback() {
                @Override
                public void connectionLost(Throwable cause) {
                    System.err.println("Connexion perdue : " + cause.getMessage());
                }

                @Override
                public void messageArrived(String topic, MqttMessage message) {
                    handleServerMessage(topic, new String(message.getPayload()));
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken token) {
                    // Optionnel : message livré
                }
            });
            mqttClient.connect();
            mqttClient.subscribe(gameTopic);
            mqttClient.subscribe(playerTopicPrefix + nickname);
            System.out.println("Connecté au serveur MQTT avec le pseudo : " + nickname);
        } catch (MqttException e) {
            System.err.println("Erreur lors de la connexion au serveur MQTT : " + e.getMessage());
        }
    }

    /**
     * Gère les messages reçus du serveur.
     *
     * @param topic   Le sujet du message (général ou privé).
     * @param message Le contenu du message reçu.
     */
    private void handleServerMessage(String topic, String message) {
        if (topic.equals(gameTopic)) {
            System.out.println("Message du serveur : " + message);
        } else if (topic.equals(playerTopicPrefix + nickname)) {
            System.out.println("Message privé : " + message);
        } else {
            System.out.println("Message inconnu reçu : " + message);
        }
    }

    /**
     * Permet au joueur d'interagir avec le serveur via des commandes.
     */
    public void interactWithServer() {
        Scanner scanner = new Scanner(System.in);
        try {
            // Envoyer le message d'inscription
            sendJoinMessage();

            // Boucle d'interaction
            while (true) {
                System.out.println("Options : START (lancer la partie), QUIT (quitter)");
                String command = scanner.nextLine().trim().toUpperCase();

                if ("QUIT".equals(command)) {
                    System.out.println("Déconnexion...");
                    mqttClient.disconnect();
                    break;
                } else if ("START".equals(command)) {
                    sendStartCommand();
                } else {
                    System.out.println("Commande inconnue !");
                }
            }
        } catch (MqttException e) {
            System.err.println("Erreur lors de l'interaction avec le serveur : " + e.getMessage());
        } finally {
            scanner.close();
        }
    }

    /**
     * Envoie un message au serveur pour rejoindre la partie.
     */
    private void sendJoinMessage() {
        try {
            mqttClient.publish(gameTopic, new MqttMessage(("JOIN:" + nickname).getBytes()));
            System.out.println("Vous avez rejoint la partie avec le pseudo : " + nickname);
        } catch (MqttException e) {
            System.err.println("Erreur lors de l'envoi du message JOIN : " + e.getMessage());
        }
    }

    /**
     * Envoie une commande START au serveur pour lancer une partie.
     */
    private void sendStartCommand() {
        try {
            mqttClient.publish(gameTopic, new MqttMessage("START".getBytes()));
        } catch (MqttException e) {
            System.err.println("Erreur lors de l'envoi de la commande START : " + e.getMessage());
        }
    }
}
