
public class Livre extends Media implements Cloneable{

    private String auteur;
    private String reference;

    public Livre(){
        super();
        auteur = "inconnue";
        reference = "ISBN0001151";
    }

    public Livre(Livre l){
        super(l);
        this.auteur = l.auteur;
        this.reference = l.reference;
    }

    public Livre(String t, StringBuffer c, int n, String a, String r){
        super(t,c,n);
        auteur = a;
        reference = r;
    }

    public String getAuteur(){return auteur;}
    public String getReference(){return reference;}

    public void setReference(String ref) {this.reference = ref;}

    
    @Override
    public Livre clone() throws CloneNotSupportedException{
        Livre clone = (Livre)super.clone();
        return clone;
    }

    @Override
    public String toString() {
        return super.toString() + ", Auteur: " + auteur + ", Reference: " + reference;
    }

    public static void main(String[] args) {
        Media.SetNom("Médiathèque Universelle");
        Livre livre = new Livre("Le livre de la jungle", new StringBuffer("DFTY4HG"), 5, "R. Kippling", "2081263246");
        System.out.println(livre);
    }

}