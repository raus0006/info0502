import java.util.Vector;

public class Mediateque implements Cloneable{

    private String nom;
    private Vector<Media> medias;

    public Mediateque(){
        medias = new Vector<>();
        nom = "Inconnu";
    }

    public Mediateque(Vector<Media> m , String d, String n){
        medias = new Vector<>(m);
        nom = n;
    }

    public Mediateque(Mediateque autre) {
        this.nom= autre.nom;
        this.medias = new Vector<>(autre.medias);
    }


    public void add(Media m){
        medias.add(m);
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Médiathèque de " + nom + " :\n");
        for (Media m : medias) {
            sb.append(m.toString()).append("\n");
        }
        return sb.toString();
    }
}

