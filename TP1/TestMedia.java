public class TestMedia {
    public static void main(String[] args) {
        Media.SetNom("Médiathèque Universelle");

        Livre livre1 = new Livre("Le livre de la jungle", new StringBuffer("DFTY4HG"), 5, "R. Kippling", "2081263246");

        String[] acteurs = {"Leonardo DiCaprio", "Joseph Gordon-Levitt", "Ellen Page"};
        Film film1 = new Film("Inception", new StringBuffer("XZ12ER"), 8, acteurs, "Christopher Nolan", 2010);

        Mediateque mediatheque = new Mediateque();
        mediatheque.add(livre1);
        mediatheque.add(film1);

        System.out.println(mediatheque);

        livre1.SetCote(new StringBuffer("SD77DS"));
        System.out.println("Livre modifié : " + livre1);

        Film film2 = new Film(film1);
        System.out.println("Film copié : " + film2);
    }
}
