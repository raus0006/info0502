public class Media implements Cloneable{
    private String titre;
    private StringBuffer cote;
    private int note;

    private static String nom;

    public Media(){
        titre = "LeMedia";
        cote = new StringBuffer("LaCote");
        note = 10;
    }

    public Media(String t, StringBuffer c, int n){
        titre = t; 
        cote = c;
        note =n;
    }

    public String getNom(){
        return nom;
    }
    

    public String getTitre(){
        return titre;
    }

    public StringBuffer getCote(){
        return new StringBuffer(cote);
    }
    

    public int getNote(){
        return note;
    }

    public Media(Media m){
        this.titre = m.getTitre();
        this.cote = new StringBuffer(m.getCote());
        this.note = m.getNote();
    }

   

    public void SetCote(StringBuffer c){
        cote = c;
    }

    public void SetNote(int n){
        note = n;
    }

    public static void SetNom(String n){
        nom = n;
    }
    
    
    public void DefautNom(){
        nom = "Defaut";
    }

    
    public Media clone() throws CloneNotSupportedException{
        Media clone = (Media) super.clone();
        clone.cote = new StringBuffer(this.cote);
        return clone;
    }

    
    public boolean equals(Media m){
        boolean res = true;
        if(this.note != m.note){
            res = false;
        }
        if(!this.titre.equals(m.titre)){
            res = false;
        }
        if(!this.cote.toString().equals(m.cote.toString())){
            res = false;
        }
        return res;
    }

    public String toString(){
        return nom + " = titre : " + titre + "; cote : " + cote + "; note : " + note;
    }
    


}