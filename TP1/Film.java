public class Film extends Media implements Cloneable{

    private String realisateur;
    private String acteur[];
    private int annee;

    public String getRealisateur(){return realisateur;}
    public String[] getActeur(){return acteur.clone();}

    public Film(){
        super();
        realisateur = "inconnue";
        acteur = new String[]{"personne", "personne1", "personne3"};
        this.annee = 0;
    }

    public Film(Film f){
        super(f.getTitre(), f.getCote(), f.getNote());
        this.realisateur = f.realisateur;
        this.acteur = f.acteur.clone();
        this.annee = f.annee;
    }

    public Film(String t, StringBuffer c, int n, String[] act, String r, int a){
        super(t,c,n);
        realisateur = r;
        acteur = act;
        annee = a;
    }

   
    @Override
    public Film clone() throws CloneNotSupportedException{
        Film clone = (Film) super.clone();
        clone.acteur = this.acteur.clone(); 
        return clone;
    }

    @Override
    public String toString() {
        return super.toString() + ", Réalisateur: " + realisateur + ", Année: " + annee;
    }

    public static void main(String[] args) {
        String[] acteurs = {"Leonardo DiCaprio", "Joseph Gordon-Levitt", "Ellen Page"};
        Film film = new Film("Inception", new StringBuffer("XZ12ER"), 8, acteurs, "Christopher Nolan", 2010);
        System.out.println(film);
    }

}