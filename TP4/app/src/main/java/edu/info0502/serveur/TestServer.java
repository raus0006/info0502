package edu.info0502.serveur;

/**
 * Classe principale pour démarrer le serveur RabbitMQ.
 */
public class TestServer {

    /**
     * Point d'entrée de l'application serveur.
     * 
     * @param args Arguments de la ligne de commande (non utilisés).
     */
    public static void main(String[] args) {
        try {
            // Initialisation et démarrage du serveur RabbitMQ
            RabbitMQServer server = new RabbitMQServer();
            server.start();
        } catch (Exception e) {
            // Gestion des erreurs lors du démarrage du serveur
            System.err.println("[Error] Échec du lancement du serveur : " + e.getMessage());
        }
    }
}
