package edu.info0502.serveur;

import java.util.Map;

import edu.info0502.commun.QCM;
import edu.info0502.commun.Question;
import edu.info0502.commun.Resultat;

/**
 * Classe responsable de la correction d'un QCM en fonction des réponses de l'utilisateur.
 */
public class QCMCorrector {
    private final QCMManager qcmManager;

    /**
     * Constructeur : initialise le correcteur avec un gestionnaire de QCM.
     *
     * @param qcmManager Instance du gestionnaire de QCM.
     */
    public QCMCorrector(QCMManager qcmManager) {
        this.qcmManager = qcmManager;
    }

    /**
     * Corrige un QCM et calcule les résultats basés sur les réponses utilisateur.
     *
     * @param qcmId     L'identifiant du QCM.
     * @param responses Map contenant les réponses de l'utilisateur 
     *                  (clé : ID de la question, valeur : réponse).
     * @return Un objet {@link Resultat} contenant les scores obtenus.
     */
    public Resultat correctQCM(String qcmId, Map<String, String> responses) {
        // Récupération du QCM via son ID
        QCM qcm = qcmManager.getQCM(qcmId);

        // Vérification si le QCM existe
        if (qcm == null) {
            System.err.println("[Erreur] QCM introuvable avec l'ID : " + qcmId);
            return new Resultat(0, 0, 0); // Retourne un résultat vide si non trouvé
        }

        // Initialisation des compteurs
        int correct = 0;
        int wrong = 0;
        int unanswered = 0;

        // Parcours des questions du QCM
        for (Question question : qcm.getQuestions()) {
            String userAnswer = responses.get(question.getId()); // Réponse utilisateur pour la question

            // Si aucune réponse n'est donnée
            if (userAnswer == null) {
                unanswered++;
            }
            // Si la réponse est correcte
            else if (userAnswer.equals(question.getCorrectAnswer())) {
                correct++;
            }
            // Si la réponse est incorrecte
            else {
                wrong++;
            }
        }

        // Création du résultat final
        return new Resultat(correct, wrong, unanswered);
    }
}
