package edu.info0502.serveur;

import com.rabbitmq.client.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.info0502.commun.QCM;
import edu.info0502.commun.Resultat;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * Serveur RabbitMQ pour gérer les inscriptions, les demandes de QCM et les réponses.
 */
public class RabbitMQServer {

    private static final String REGISTRATION_QUEUE = "user_registration";
    private static final String QCM_REQUEST_QUEUE = "qcm_requests";
    private static final String ANSWER_QUEUE = "qcm_answers";
    private static final String RESULT_QUEUE = "qcm_results";

    private final UserManager userManager;
    private final QCMManager qcmManager;
    private final QCMCorrector qcmCorrector;

    /**
     * Constructeur : initialise les gestionnaires de QCM, utilisateurs et correcteurs.
     */
    public RabbitMQServer() {
        this.userManager = new UserManager();
        this.qcmManager = new QCMManager();
        this.qcmCorrector = new QCMCorrector(qcmManager);
    }

    /**
     * Démarre le serveur RabbitMQ et configure les canaux pour traiter les messages.
     *
     * @throws Exception En cas d'erreur de connexion ou de configuration.
     */
    public void start() throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("10.11.17.215");
        factory.setUsername("admin");
        factory.setPassword("12345678");

        try (Connection connection = factory.newConnection()) {
            System.out.println(" [*] Serveur RabbitMQ en attente de messages...");

            // Configuration des différents canaux
            Channel registrationChannel = connection.createChannel();
            configureRegistrationChannel(registrationChannel);

            Channel qcmRequestChannel = connection.createChannel();
            configureQCMRequestChannel(qcmRequestChannel);

            Channel answersChannel = connection.createChannel();
            configureAnswersChannel(answersChannel);

            // Maintenir le serveur actif
            synchronized (this) {
                this.wait();
            }
        }
    }

    /**
     * Configure le canal pour traiter les messages d'inscription.
     *
     * @param channel Le canal RabbitMQ.
     * @throws Exception En cas d'erreur.
     */
    private void configureRegistrationChannel(Channel channel) throws Exception {
        channel.queueDeclare(REGISTRATION_QUEUE, false, false, false, null);
        channel.basicConsume(REGISTRATION_QUEUE, true, (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            handleRegistration(message);
        }, consumerTag -> {});
    }

    /**
     * Configure le canal pour traiter les demandes de QCM.
     *
     * @param channel Le canal RabbitMQ.
     * @throws Exception En cas d'erreur.
     */
    private void configureQCMRequestChannel(Channel channel) throws Exception {
        channel.queueDeclare(QCM_REQUEST_QUEUE, false, false, false, null);
        channel.basicConsume(QCM_REQUEST_QUEUE, true, (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            handleQCMRequest(message, channel);
        }, consumerTag -> {});
    }

    /**
     * Configure le canal pour traiter les réponses aux QCM.
     *
     * @param channel Le canal RabbitMQ.
     * @throws Exception En cas d'erreur.
     */
    private void configureAnswersChannel(Channel channel) throws Exception {
        channel.queueDeclare(ANSWER_QUEUE, false, false, false, null);
        channel.basicConsume(ANSWER_QUEUE, true, (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            handleAnswers(message, channel);
        }, consumerTag -> {});
    }

    /**
     * Traite les messages d'inscription reçus.
     *
     * @param message Le message JSON contenant les informations d'inscription.
     */
    private void handleRegistration(String message) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            Map<String, String> data = mapper.readValue(message, HashMap.class);

            String username = data.get("username");
            String password = data.get("password");
            String email = data.get("email");

            String result = userManager.addUser(username, password, email);
            System.out.println("[Registration] " + result);
        } catch (Exception e) {
            System.err.println("[Error] Erreur lors de l'inscription : " + e.getMessage());
        }
    }

    /**
     * Traite les demandes de QCM reçues.
     *
     * @param message Le message JSON contenant l'ID du QCM.
     * @param channel Le canal RabbitMQ pour répondre.
     */
    private void handleQCMRequest(String message, Channel channel) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            Map<String, String> data = mapper.readValue(message, HashMap.class);

            String qcmId = data.get("qcm_id");
            QCM qcm = qcmManager.getQCM(qcmId);

            if (qcm != null) {
                String responseQueue = data.get("response_queue");
                String qcmJson = mapper.writeValueAsString(qcm);
                channel.basicPublish("", responseQueue, null, qcmJson.getBytes(StandardCharsets.UTF_8));
                System.out.println("[QCM Request] QCM envoyé à la file : " + responseQueue);
            } else {
                System.err.println("[Error] QCM introuvable : " + qcmId);
            }
        } catch (Exception e) {
            System.err.println("[Error] Erreur lors de la demande de QCM : " + e.getMessage());
        }
    }

    /**
     * Traite les réponses aux QCM reçues.
     *
     * @param message Le message JSON contenant les réponses.
     * @param channel Le canal RabbitMQ pour répondre avec les résultats.
     */
    private void handleAnswers(String message, Channel channel) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            Map<String, Object> data = mapper.readValue(message, new TypeReference<Map<String, Object>>() {});

            String qcmId = (String) data.get("qcm_id");
            Map<String, String> responses = (Map<String, String>) data.get("responses");
            String resultQueue = (String) data.get("result_queue");

            Resultat result = qcmCorrector.correctQCM(qcmId, responses);
            String resultJson = mapper.writeValueAsString(result);

            channel.basicPublish("", resultQueue, null, resultJson.getBytes(StandardCharsets.UTF_8));
            System.out.println("[Debug] Résultat envoyé à la queue " + resultQueue + " : " + resultJson);
        } catch (Exception e) {
            System.err.println("[Error] Erreur lors de la gestion des réponses : " + e.getMessage());
        }
    }

    /**
     * Affiche des informations sur une file RabbitMQ.
     *
     * @param channel    Le canal RabbitMQ.
     * @param queueName  Le nom de la file.
     */
    private static void logQueueInfo(Channel channel, String queueName) {
        try {
            AMQP.Queue.DeclareOk declareOk = channel.queueDeclarePassive(queueName);
            int messageCount = declareOk.getMessageCount();
            int consumerCount = declareOk.getConsumerCount();

            System.out.println("[Debug] Queue " + queueName + " : Messages = " + messageCount + ", Consommateurs = " + consumerCount);
        } catch (Exception e) {
            System.err.println("[Error] Impossible de récupérer les informations sur la queue " + queueName + " : " + e.getMessage());
        }
    }
}
