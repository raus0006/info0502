package edu.info0502.serveur;

import com.fasterxml.jackson.databind.ObjectMapper;

import edu.info0502.commun.User;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Gestionnaire des utilisateurs. Permet la gestion des utilisateurs, y compris
 * leur création, suppression et l'ajout de scores.
 */
public class UserManager {
    private final String filePath = "users.json";
    private Map<String, User> users;

    /**
     * Constructeur : initialise le gestionnaire des utilisateurs en chargeant
     * les données depuis un fichier JSON.
     */
    public UserManager() {
        loadUsers();
    }

    /**
     * Charge les utilisateurs depuis un fichier JSON.
     * Si le fichier n'existe pas, initialise une liste vide.
     */
    private void loadUsers() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            File file = new File(filePath);
            if (file.exists()) {
                users = mapper.readValue(file, mapper.getTypeFactory()
                        .constructMapType(HashMap.class, String.class, User.class));
            } else {
                users = new HashMap<>();
            }
        } catch (IOException e) {
            System.err.println("Erreur lors du chargement des utilisateurs : " + e.getMessage());
            users = new HashMap<>();
        }
    }

    /**
     * Sauvegarde les utilisateurs dans un fichier JSON.
     */
    private void saveUsers() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writerWithDefaultPrettyPrinter().writeValue(new File(filePath), users);
        } catch (IOException e) {
            System.err.println("Erreur lors de la sauvegarde des utilisateurs : " + e.getMessage());
        }
    }

    /**
     * Ajoute un utilisateur avec le nom d'utilisateur, le mot de passe et l'email.
     *
     * @param username Nom unique de l'utilisateur
     * @param password Mot de passe de l'utilisateur
     * @param email    Adresse email de l'utilisateur
     * @return Message indiquant si l'utilisateur a été ajouté avec succès
     * ou si l'utilisateur existe déjà.
     */
    public String addUser(String username, String password, String email) {
        if (users.containsKey(username)) {
            return "Erreur : L'utilisateur existe déjà.";
        }
        users.put(username, new User(username, password, email));
        saveUsers();
        return "Utilisateur ajouté avec succès.";
    }

    /**
     * Supprime un utilisateur identifié par son nom.
     *
     * @param username Nom de l'utilisateur à supprimer
     * @return Message indiquant si l'utilisateur a été supprimé avec succès
     * ou si l'utilisateur n'a pas été trouvé.
     */
    public String deleteUser(String username) {
        if (!users.containsKey(username)) {
            return "Erreur : Utilisateur non trouvé.";
        }
        users.remove(username);
        saveUsers();
        return "Utilisateur supprimé avec succès.";
    }

    /**
     * Récupère un utilisateur en fonction de son nom.
     *
     * @param username Nom de l'utilisateur à récupérer
     * @return L'objet {@link User} correspondant, ou null si non trouvé.
     */
    public User getUser(String username) {
        return users.get(username);
    }

    /**
     * Ajoute un score pour un utilisateur donné sur un QCM.
     *
     * @param username Nom de l'utilisateur
     * @param qcmId    Identifiant unique du QCM
     * @param score    Score obtenu par l'utilisateur
     * @return Message indiquant si le score a été ajouté avec succès
     * ou si l'utilisateur n'a pas été trouvé.
     */
    public String addScore(String username, String qcmId, int score) {
        User user = users.get(username);
        if (user == null) {
            return "Erreur : Utilisateur non trouvé.";
        }
        user.getScores().put(qcmId, score);
        saveUsers();
        return "Score ajouté avec succès.";
    }

    /**
     * Réinitialise la liste des utilisateurs en supprimant toutes les données.
     */
    public void resetUsers() {
        users = new HashMap<>();
        saveUsers();
        System.out.println("Tous les utilisateurs ont été supprimés.");
    }
}
