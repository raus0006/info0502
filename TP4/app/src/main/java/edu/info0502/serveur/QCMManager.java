package edu.info0502.serveur;

import com.fasterxml.jackson.databind.ObjectMapper;

import edu.info0502.commun.QCM;
import edu.info0502.commun.Question;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Classe pour gérer les QCM : permet de les ajouter, de les récupérer et de les sauvegarder dans un fichier JSON.
 */
public class QCMManager {
    private final String filePath = "qcm.json"; // Chemin du fichier JSON de sauvegarde
    private Map<String, QCM> qcms;

    /**
     * Constructeur : initialise le gestionnaire et charge les QCM depuis un fichier JSON.
     */
    public QCMManager() {
        loadQCMs();
    }

    /**
     * Charge les QCM depuis un fichier JSON. 
     * Si le fichier n'existe pas, initialise une nouvelle structure de données.
     */
    private void loadQCMs() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            File file = new File(filePath);
            if (file.exists()) {
                qcms = mapper.readValue(file, mapper.getTypeFactory().constructMapType(HashMap.class, String.class, QCM.class));
                System.out.println("QCMs chargés avec succès depuis " + filePath);
            } else {
                qcms = new HashMap<>();
                System.out.println("Fichier non trouvé. Initialisation d'une nouvelle liste de QCMs.");
            }
        } catch (IOException e) {
            System.err.println("[Erreur] Problème lors du chargement des QCMs : " + e.getMessage());
            qcms = new HashMap<>();
        }
    }

    /**
     * Sauvegarde les QCM dans un fichier JSON.
     */
    private void saveQCMs() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writerWithDefaultPrettyPrinter().writeValue(new File(filePath), qcms);
            System.out.println("QCMs sauvegardés avec succès dans " + filePath);
        } catch (IOException e) {
            System.err.println("[Erreur] Problème lors de la sauvegarde des QCMs : " + e.getMessage());
        }
    }

    /**
     * Ajoute un nouveau QCM au gestionnaire et le sauvegarde dans le fichier JSON.
     *
     * @param qcmId    Identifiant unique du QCM.
     * @param title    Titre du QCM.
     * @param questions Liste des questions associées au QCM.
     */
    public void addQCM(String qcmId, String title, List<Question> questions) {
        if (qcms.containsKey(qcmId)) {
            System.out.println("[Info] Un QCM avec l'ID " + qcmId + " existe déjà. Mise à jour des informations.");
        }
        qcms.put(qcmId, new QCM(qcmId, title, questions));
        saveQCMs();
    }

    /**
     * Récupère un QCM à partir de son identifiant.
     *
     * @param qcmId Identifiant unique du QCM.
     * @return Le QCM correspondant ou null s'il n'existe pas.
     */
    public QCM getQCM(String qcmId) {
        if (!qcms.containsKey(qcmId)) {
            System.err.println("[Erreur] Aucun QCM trouvé avec l'ID : " + qcmId);
            return null;
        }
        return qcms.get(qcmId);
    }
}
