package edu.info0502.commun;

/**
 * Classe représentant les résultats d'un QCM (Questionnaire à Choix Multiples).
 * Elle contient le nombre de réponses correctes, incorrectes et non répondues.
 */
public class Resultat {
    private int correct; // Nombre de réponses correctes
    private int wrong; // Nombre de réponses incorrectes
    private int unanswered; // Nombre de questions sans réponse

    /**
     * Constructeur pour initialiser les résultats d'un QCM.
     *
     * @param correct Nombre de réponses correctes.
     * @param wrong Nombre de réponses incorrectes.
     * @param unanswered Nombre de questions sans réponse.
     */
    public Resultat(int correct, int wrong, int unanswered) {
        this.correct = correct;
        this.wrong = wrong;
        this.unanswered = unanswered;
    }

    /**
     * Retourne une représentation en chaîne de caractères des résultats.
     *
     * @return Une chaîne contenant les résultats sous forme lisible.
     */
    @Override
    public String toString() {
        return "Correct: " + correct + ", Wrong: " + wrong + ", Unanswered: " + unanswered;
    }

    /**
     * Obtient le nombre de réponses correctes.
     *
     * @return Le nombre de réponses correctes.
     */
    public int getCorrect() {
        return correct;
    }

    /**
     * Définit le nombre de réponses correctes.
     *
     * @param correct Le nombre de réponses correctes à définir.
     */
    public void setCorrect(int correct) {
        this.correct = correct;
    }

    /**
     * Obtient le nombre de réponses incorrectes.
     *
     * @return Le nombre de réponses incorrectes.
     */
    public int getWrong() {
        return wrong;
    }

    /**
     * Définit le nombre de réponses incorrectes.
     *
     * @param wrong Le nombre de réponses incorrectes à définir.
     */
    public void setWrong(int wrong) {
        this.wrong = wrong;
    }

    /**
     * Obtient le nombre de questions sans réponse.
     *
     * @return Le nombre de questions sans réponse.
     */
    public int getUnanswered() {
        return unanswered;
    }

    /**
     * Définit le nombre de questions sans réponse.
     *
     * @param unanswered Le nombre de questions sans réponse à définir.
     */
    public void setUnanswered(int unanswered) {
        this.unanswered = unanswered;
    }
}
