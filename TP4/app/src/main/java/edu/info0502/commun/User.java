package edu.info0502.commun;

import java.util.HashMap;
import java.util.Map;

/**
 * Classe représentant un utilisateur avec un nom, un mot de passe, un email, 
 * et une liste de scores associés aux QCM réalisés.
 */
public class User {
    private String username; // Nom d'utilisateur
    private String password; // Mot de passe
    private String email; // Adresse email
    private Map<String, Integer> scores; // Scores des QCM réalisés (clé : ID du QCM, valeur : score)

    /**
     * Constructeur par défaut (requis pour la sérialisation avec Jackson).
     * Initialise un utilisateur avec des valeurs par défaut.
     */
    public User() {
        this.username = "";
        this.password = "";
        this.email = "";
        this.scores = new HashMap<>();
    }

    /**
     * Constructeur pour initialiser un utilisateur avec des valeurs spécifiques.
     *
     * @param username Nom d'utilisateur.
     * @param password Mot de passe.
     * @param email Adresse email.
     */
    public User(String username, String password, String email) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.scores = new HashMap<>();
    }

    /**
     * Obtient le nom d'utilisateur.
     *
     * @return Le nom d'utilisateur.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Définit le nom d'utilisateur.
     *
     * @param username Le nom d'utilisateur à définir.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Obtient le mot de passe de l'utilisateur.
     *
     * @return Le mot de passe.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Définit le mot de passe de l'utilisateur.
     *
     * @param password Le mot de passe à définir.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Obtient l'adresse email de l'utilisateur.
     *
     * @return L'adresse email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Définit l'adresse email de l'utilisateur.
     *
     * @param email L'adresse email à définir.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Obtient les scores de l'utilisateur pour les QCM réalisés.
     *
     * @return Une map contenant les scores (clé : ID du QCM, valeur : score).
     */
    public Map<String, Integer> getScores() {
        return scores;
    }

    /**
     * Ajoute ou met à jour un score pour un QCM spécifique.
     *
     * @param qcmId ID du QCM.
     * @param score Score obtenu.
     */
    public void addScore(String qcmId, int score) {
        this.scores.put(qcmId, score);
    }

    /**
     * Supprime un score pour un QCM spécifique.
     *
     * @param qcmId ID du QCM à supprimer.
     */
    public void removeScore(String qcmId) {
        this.scores.remove(qcmId);
    }

    /**
     * Représente l'utilisateur sous forme de chaîne de caractères.
     *
     * @return Une chaîne décrivant l'utilisateur.
     */
    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", scores=" + scores +
                '}';
    }
}
