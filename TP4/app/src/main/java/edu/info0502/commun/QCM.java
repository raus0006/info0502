package edu.info0502.commun;

import java.util.List;

/**
 * Représente un QCM (Questionnaire à Choix Multiples) avec un identifiant unique,
 * un titre descriptif, et une liste de questions associées.
 */
public class QCM {
    private String id; // Identifiant unique du QCM
    private String title; // Titre descriptif du QCM
    private List<Question> questions; // Liste des questions du QCM

    /**
     * Constructeur par défaut.
     * Requis pour les frameworks de sérialisation comme Jackson.
     */
    public QCM() {}

    /**
     * Constructeur pour initialiser un QCM avec les paramètres fournis.
     *
     * @param id L'identifiant unique du QCM.
     * @param title Le titre descriptif du QCM.
     * @param questions La liste des questions associées au QCM.
     */
    public QCM(String id, String title, List<Question> questions) {
        this.id = id;
        this.title = title;
        this.questions = questions;
    }

    /**
     * Obtient la liste des questions du QCM.
     *
     * @return Une liste de {@link Question}.
     */
    public List<Question> getQuestions() {
        return questions;
    }

    /**
     * Définit la liste des questions du QCM.
     *
     * @param questions La liste des questions à associer au QCM.
     */
    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    /**
     * Obtient l'identifiant unique du QCM.
     *
     * @return L'identifiant du QCM.
     */
    public String getId() {
        return id;
    }

    /**
     * Définit l'identifiant unique du QCM.
     *
     * @param id L'identifiant à associer au QCM.
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Obtient le titre descriptif du QCM.
     *
     * @return Le titre du QCM.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Définit le titre descriptif du QCM.
     *
     * @param title Le titre à associer au QCM.
     */
    public void setTitle(String title) {
        this.title = title;
    }
}
