package edu.info0502.commun;

import java.util.List;

/**
 * Représente une question d'un QCM (Questionnaire à Choix Multiples).
 * Chaque question est identifiée par un ID unique, possède un texte descriptif,
 * une liste de choix possibles, et une réponse correcte.
 */
public class Question {
    private String id; // Identifiant unique de la question
    private String text; // Texte de la question
    private List<String> choices; // Liste des choix possibles pour la question
    private String correctAnswer; // Réponse correcte pour la question

    /**
     * Constructeur par défaut.
     * Requis pour les frameworks de sérialisation comme Jackson.
     */
    public Question() {}

    /**
     * Constructeur pour initialiser une question avec les paramètres fournis.
     *
     * @param id L'identifiant unique de la question.
     * @param text Le texte descriptif de la question.
     * @param choices La liste des choix possibles.
     * @param correctAnswer La réponse correcte.
     */
    public Question(String id, String text, List<String> choices, String correctAnswer) {
        this.id = id;
        this.text = text;
        this.choices = choices;
        this.correctAnswer = correctAnswer;
    }

    /**
     * Obtient l'identifiant unique de la question.
     *
     * @return L'identifiant de la question.
     */
    public String getId() {
        return id;
    }

    /**
     * Définit l'identifiant unique de la question.
     *
     * @param id L'identifiant à associer à la question.
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Obtient le texte descriptif de la question.
     *
     * @return Le texte de la question.
     */
    public String getText() {
        return text;
    }

    /**
     * Définit le texte descriptif de la question.
     *
     * @param text Le texte à associer à la question.
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * Obtient la liste des choix possibles pour la question.
     *
     * @return Une liste de choix.
     */
    public List<String> getChoices() {
        return choices;
    }

    /**
     * Définit la liste des choix possibles pour la question.
     *
     * @param choices Les choix à associer à la question.
     */
    public void setChoices(List<String> choices) {
        this.choices = choices;
    }

    /**
     * Obtient la réponse correcte de la question.
     *
     * @return La réponse correcte.
     */
    public String getCorrectAnswer() {
        return correctAnswer;
    }

    /**
     * Définit la réponse correcte de la question.
     *
     * @param correctAnswer La réponse correcte à associer à la question.
     */
    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }
}
