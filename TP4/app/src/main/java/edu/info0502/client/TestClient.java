package edu.info0502.client;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection; // Import explicite pour éviter les conflits

import java.nio.charset.StandardCharsets;

/**
 * Classe principale pour tester les fonctionnalités du client RabbitMQ.
 * Cette classe simule un workflow complet, y compris l'inscription d'un utilisateur,
 * la demande de QCM, la soumission des réponses, et la réception des résultats.
 */
public class TestClient {
    /**
     * Méthode principale pour exécuter les tests.
     *
     * @param args Arguments de la ligne de commande.
     * @throws Exception Si une erreur survient pendant l'exécution.
     */
    public static void main(String[] args) throws Exception {

        // Test de connexion à RabbitMQ
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("10.11.17.215");
        factory.setUsername("admin");
        factory.setPassword("12345678");
        try (Connection connection = factory.newConnection()) { // Utilisation explicite
            System.out.println("Connexion réussie !");
        } catch (Exception e) {
            System.err.println("Erreur de connexion : " + e.getMessage());
        }

        // Inscription d'un utilisateur
        System.out.println(" [Test] Envoi du message d'inscription...");
        String registrationMessage = "{ \"username\": \"test_user2\", \"password\": \"password123\", \"email\": \"test2@example.com\" }";
        RabbitMQClient.sendMessage("user_registration", registrationMessage);

        // Demande d'un QCM
        System.out.println(" [Test] Envoi de la demande de QCM...");
        String qcmRequestMessage = "{ \"qcm_id\": \"12345\", \"response_queue\": \"qcm_results\" }";
        RabbitMQClient.sendMessage("qcm_requests", qcmRequestMessage);

        // Réponses d'un utilisateur
        System.out.println(" [Test] Envoi des réponses au QCM...");
        String answerMessage = "{ \"qcm_id\": \"12345\", \"responses\": { \"1\": \"Paris\", \"2\": \"5\" }, \"result_queue\": \"qcm_results\" }";
        RabbitMQClient.sendMessage("qcm_answers", answerMessage);

        // Attente des résultats
        System.out.println(" [Test] Attente des résultats du QCM...");
        Thread.sleep(2000); // Attente pour que le serveur traite les réponses
        RabbitMQClient.receiveMessage("qcm_results");
    }
}
