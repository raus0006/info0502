package edu.info0502.client;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import java.nio.charset.StandardCharsets;

/**
 * Classe utilitaire pour interagir avec RabbitMQ.
 * Elle permet l'envoi et la réception de messages dans des queues spécifiques.
 */
public class RabbitMQClient {
    private static final String REGISTRATION_QUEUE = "user_registration";
    private static final String QCM_REQUEST_QUEUE = "qcm_requests";
    private static final String ANSWER_QUEUE = "qcm_answers";
    private static final String RESULT_QUEUE = "qcm_results";

    /**
     * Envoie un message à une queue RabbitMQ spécifiée.
     *
     * @param queueName Nom de la queue où le message sera envoyé.
     * @param message   Contenu du message à envoyer.
     * @throws Exception Si une erreur survient lors de la connexion ou de l'envoi du message.
     */
    public static void sendMessage(String queueName, String message) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("10.11.17.215"); // mettre l'adresse ip du serveur
        factory.setUsername("admin");
        factory.setPassword("12345678");

        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {

            // Déclare la queue si elle n'existe pas
            channel.queueDeclare(queueName, false, false, false, null);

            // Publie le message dans la queue
            channel.basicPublish("", queueName, null, message.getBytes(StandardCharsets.UTF_8));

            System.out.println(" [x] Message envoyé à la file : " + queueName);
        }
    }

    /**
     * Reçoit un message à partir d'une queue RabbitMQ spécifiée.
     * Cette méthode reste en attente jusqu'à la réception d'un message.
     *
     * @param queueName Nom de la queue à écouter pour les messages entrants.
     * @throws Exception Si une erreur survient lors de la connexion ou de la réception.
     */
    public static void receiveMessage(String queueName) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("10.11.17.215"); // mettre l'adresse ip du serveur
        factory.setUsername("admin");
        factory.setPassword("12345678");

        final Object lock = new Object(); // Verrou explicite pour synchronisation

        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {

            // Déclare la queue si elle n'existe pas
            channel.queueDeclare(queueName, false, false, false, null);
            System.out.println("[Debug] Client en attente de messages sur la queue : " + queueName);

            // Définit une action lorsque des messages sont reçus
            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                try {
                    String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
                    System.out.println("[Debug] Résultat reçu : " + message);

                    synchronized (lock) {
                        lock.notify(); // Libère l'attente une fois qu'un message est reçu
                    }
                } catch (Exception e) {
                    System.err.println("[Error] Erreur lors de la réception du message : " + e.getMessage());
                }
            };

            // Démarre la consommation des messages de la queue
            channel.basicConsume(queueName, true, deliverCallback, consumerTag -> {
                System.out.println("[Debug] Consommateur annulé : " + consumerTag);
            });

            // Bloque l'exécution pour permettre la réception
            synchronized (lock) {
                lock.wait(); // Attente active jusqu'à notification
            }
        }
    }
}
