package edu.info0502.client;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import edu.info0502.commun.Question;
import edu.info0502.commun.Resultat;
import edu.info0502.serveur.QCMCorrector;
import edu.info0502.serveur.QCMManager;
import edu.info0502.serveur.UserManager;

public class Test {
    public static void main(String[] args) {
        UserManager userManager = new UserManager();
        QCMManager qcmManager = new QCMManager();
        QCMCorrector corrector = new QCMCorrector(qcmManager);

        // Ajout d'un utilisateur
        System.out.println(userManager.addUser("test_user1", "password123", "Marc@example.com"));

        // Ajout d'un QCM
        qcmManager.addQCM("12345", "Connaissances Générales", Arrays.asList(
                new Question("1", "Quelle est la capitale de la France ?", Arrays.asList("Paris", "Lyon", "Marseille"), "Paris"),
                new Question("2", "Combien y a-t-il de continents ?", Arrays.asList("5", "6", "7"), "7")
        ));

        // Réponses utilisateur
        Map<String, String> responses = new HashMap<>();
        responses.put("1", "Paris");
        responses.put("2", "5");

        // Correction
        Resultat result = corrector.correctQCM("12345", responses);
        System.out.println(result);

        // Ajout du score au profil utilisateur
        System.out.println(userManager.addScore("test_user", "12345", result.getCorrect()));
    }
}
