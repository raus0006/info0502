package edu.info0502.tp2;

public class TestMains {

    public static void main(String[] args) {
        // Création d'un talon
        Talon talon = new Talon();

        // Création de deux mains depuis le talon
        Mains main1 = new Mains();
        Mains main2 = new Mains();

        // Affichage des mains
        System.out.println("Main 1 :");
        System.out.println(main1);
        System.out.println("Main 2 :");
        System.out.println(main2);

        // Évaluation des mains
        System.out.println("Score de la Main 1 : " + main1.evaluer());
        System.out.println("Score de la Main 2 : " + main2.evaluer());

        // Affichage des combinaisons
        System.out.println("Combinaison de la Main 1 : " + main1.afficherCombinaison());
        System.out.println("Combinaison de la Main 2 : " + main2.afficherCombinaison());
    }
}
