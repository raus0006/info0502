package edu.info0502.tp1;

import java.util.Vector;

/**
 * Représente une médiathèque contenant une collection de médias (livres, films).
 */

public class Mediateque implements Cloneable{

    private String nom; //nom de la médiathèque
    private Vector<Media> medias; //collection de médias dans la médiatèque


    /**
     * Constructeur par défaut.
     * Initialise une médiathèque avec un nom et une collection vides.
     */
    public Mediateque(){
        medias = new Vector<>();
        nom = "Inconnu";
    }

    /**
     * Constructeur avec paramètres.
     * Permet de définir une collection de médias et un nom.
     *
     * @param m La collection de médias.
     * @param n Le nom de la médiathèque.
     */
    public Mediateque(Vector<Media> m , String n){
        medias = new Vector<>(m);
        nom = n;
    }

     /**
     * Constructeur par copie.
     * Crée une nouvelle instance de Mediateque en copiant une autre médiathèque.
     *
     * @param autre La médiathèque à copier.
     */
    public Mediateque(Mediateque autre) {
        this.nom= autre.nom;
        this.medias = new Vector<>(autre.medias);
    }

     /**
     * Ajoute un média à la médiathèque.
     *
     * @param m Le média à ajouter.
     */
    public void add(Media m){
        medias.add(m);
    }

    /**
     * Retourne une représentation textuelle de la médiathèque.
     *
     * @return Une chaîne décrivant les médias dans la médiathèque.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Médiathèque de " + nom + " :\n");
        for (Media m : medias) {
            sb.append(m.toString()).append("\n");
        }
        return sb.toString();
    }
}

