package edu.info0502.tp2;

public class PartiePoker {

    public static void main(String[] args) {
        // Création d'un talon
        Talon talon = new Talon();
        talon.melanger();

        // Création des mains pour 4 joueurs
        Mains[] joueurs = new Mains[4];
        for (int i = 0; i < 4; i++) {
            joueurs[i] = new Mains();
        }

        // Distribution des cartes une par une
        for (int tour = 0; tour < 5; tour++) {
            for (int joueur = 0; joueur < 4; joueur++) {
                joueurs[joueur].getMain()[tour] = talon.tirerCarte();
            }
        }

        // Affichage des mains des joueurs
        for (int i = 0; i < 4; i++) {
            System.out.println("Main du Joueur " + (i + 1) + " :");
            System.out.println(joueurs[i]);
            System.out.println("Combinaison : " + joueurs[i].afficherCombinaison());
        }

        // Comparaison des mains pour trouver le vainqueur
        int meilleurJoueur = 0;
        for (int i = 1; i < 4; i++) {
            if (joueurs[i].evaluer() > joueurs[meilleurJoueur].evaluer()) {
                meilleurJoueur = i;
            }
        }

        System.out.println("Le gagnant est le Joueur " + (meilleurJoueur + 1) + " !");
    }
}
