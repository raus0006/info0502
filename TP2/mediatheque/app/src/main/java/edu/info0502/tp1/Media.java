package edu.info0502.tp1;

/**
 * Classe représentant un média dans une médiathèque.
 * Elle sert de classe de base pour les autres types de médias.
 */
public class Media implements Cloneable{
    private String titre; //titre du Média
    private StringBuffer cote; // cote du Média
    private int note;// note du Média

    private static String nom; //nom de la mediathèque

     /**
     * Constructeur par défaut de la classe Media.
     */
    public Media(){
        titre = "LeMedia";
        cote = new StringBuffer("LaCote");
        note = 10;
    }

    /**
     * Constructeur avec paramètres.
     *
     * @param t Le titre du média
     * @param c La cote du média
     * @param n La note attribuée au média
     */
    public Media(String t, StringBuffer c, int n){
        titre = t; 
        cote = c;
        note =n;
    }

    /**
     * Retourne le nom de la médiathèque.
     *
     * @return Le nom de la médiathèque.
     */
    public String getNom(){
        return nom;
    }
    
    /**
     * Retourne le titre du média.
     *
     * @return Le titre
     */
    public String getTitre(){
        return titre;
    }

    /**
     * Retourne la cote du média.
     *
     * @return La cote
     */
    public StringBuffer getCote(){
        return new StringBuffer(cote);
    }
    
     /**
     * Retourne la note du média.
     *
     * @return la note
     */
    public int getNote(){
        return note;
    }

    /**
     * Constructeur par copie.
     *
     * @param m Le média a copié
     */
    public Media(Media m){
        this.titre = m.getTitre();
        this.cote = new StringBuffer(m.getCote());
        this.note = m.getNote();
    }

   
    /**
     * Modifie la cote attribuée au média.
     *
     * @param c La nouvelle cote du média.
     */
    public void SetCote(StringBuffer c){
        cote = c;
    }

    /**
     * Modifie la note attribuée au média.
     *
     * @param n La nouvelle note du média. Doit être un entier positif ou nul.
     */
    public void SetNote(int n){
        note = n;
    }

    /**
     * Modifie le nom attribuée au média.
     *
     * @param n Le nouveau nom du média.
     */
    public static void SetNom(String n){
        nom = n;
    }
    
     /**
     * Definit par defaut le nom du média.
     *
     */
    public void DefautNom(){
        nom = "Defaut";
    }

    /**
     * Crée et retourne une copie du Media.
     * Copie profonde de l'attribut `cote`.
     *
     * @return Une nouvelle instance de Media ayant les mêmes valeurs que l'objet actuel.
     * @throws CloneNotSupportedException Si la classe Media ou une de ses super-classes ne supporte pas le clonage.
     */
    public Media clone() throws CloneNotSupportedException{
        Media clone = (Media) super.clone();
        clone.cote = new StringBuffer(this.cote);
        return clone;
    }

    /**
     * Compare le Media avec un autre pour vérifier leur égalité.
     * Deux objets Media sont considérés égaux si leurs titres, cotes et notes sont identiques.
     *
     * @param m L'objet Media à comparer.
     * @return {@code true} si les deux objets sont égaux, {@code false} sinon.
     */
    public boolean equals(Media m){
        boolean res = true;
        if(this.note != m.note){
            res = false;
        }
        if(!this.titre.equals(m.titre)){
            res = false;
        }
        if(!this.cote.toString().equals(m.cote.toString())){
            res = false;
        }
        return res;
    }

    /**
     * Retourne une représentation textuelle de l'objet Media.
     * La chaîne retournée inclut le nom de la médiathèque, le titre, la cote et la note du média.
     *
     * @return Une chaîne représentant le Media.
     */
    public String toString(){
        return nom + " = titre : " + titre + "; cote : " + cote + "; note : " + note;
    }
    


}