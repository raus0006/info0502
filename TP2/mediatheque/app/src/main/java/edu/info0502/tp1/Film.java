package edu.info0502.tp1;

/**
 * Représente un film dans une médiathèque.
 * Hérite de la classe {@link Media} et ajoute des informations spécifiques à un film,
 * comme le réalisateur, les acteurs, et l'année de sortie.
 */

public class Film extends Media implements Cloneable{

    private String realisateur; //réalisateur du film
    private String acteur[]; // acteurs principaux du film
    private int annee; // année de sortie du film

    /**
     * Retourne le réalisateur du film.
     *
     *@return Le nom du réalisateur.
     */
    public String getRealisateur(){return realisateur;}
    /**
     * Retourne une copie des acteurs du film.
     *
     * @return Un tableau contenant les noms des acteurs principaux.
     */
    public String[] getActeur(){return acteur.clone();}

     /**
     * Constructeur par défaut.
     * Initialise un film avec des valeurs par défaut.
     */
    public Film(){
        super();
        realisateur = "inconnue";
        acteur = new String[]{"personne", "personne1", "personne3"};
        this.annee = 0;
    }

    /**
     * Constructeur par copie.
     * Crée une nouvelle instance de Film en copiant les attributs d'un autre film.
     *
     * @param f Le film à copier.
     */
    public Film(Film f){
        super(f.getTitre(), f.getCote(), f.getNote());
        this.realisateur = f.realisateur;
        this.acteur = f.acteur.clone();
        this.annee = f.annee;
    }

     /**
     * Constructeur avec paramètres.
     * Permet de définir les informations spécifiques d'un film.
     *
     * @param t   Le titre du film.
     * @param c   La cote du film.
     * @param n   La note attribuée au film.
     * @param act Les acteurs principaux du film.
     * @param r   Le réalisateur du film.
     * @param a   L'année de sortie du film.
     */
    public Film(String t, StringBuffer c, int n, String[] act, String r, int a){
        super(t,c,n);
        realisateur = r;
        acteur = act;
        annee = a;
    }

   /**
     * Crée une copie du Film.
     * Réalise une copie profonde du tableau des acteurs.
     *
     * @return Une nouvelle instance de Film avec les mêmes attributs.
     * @throws CloneNotSupportedException Si la classe Film ne peut pas être clonée.
     */
    @Override
    public Film clone() throws CloneNotSupportedException{
        Film clone = (Film) super.clone();
        clone.acteur = this.acteur.clone(); 
        return clone;
    }

    /**
     * Retourne une représentation textuelle du film.
     *
     * @return Une chaîne décrivant les informations du film.
     */
    @Override
    public String toString() {
        return super.toString() + ", Réalisateur: " + realisateur + ", Année: " + annee;
    }

    /**
     * Méthode principale pour tester la classe Film.
     *
     * @param args Arguments de la ligne de commande, qui ne sert pas dans le test.
     */
    public static void main(String[] args) {
        String[] acteurs = {"Leonardo DiCaprio", "Joseph Gordon-Levitt", "Ellen Page"};
        Film film = new Film("Inception", new StringBuffer("XZ12ER"), 8, acteurs, "Christopher Nolan", 2010);
        System.out.println(film);
    }

}