package edu.info0502.tp2;

public class TestTalon {

    public static void main(String[] args) {
        // Création d'un talon
        Talon talon = new Talon();
        System.out.println("Talon initial :");
        talon.afficherToutesLesCartes();

        // Mélange des paquets dans le talon
        talon.melanger();
        System.out.println("Talon après mélange :");
        talon.afficherToutesLesCartes();

        // Tirage de cartes
        System.out.println("Carte tirée : " + talon.tirerCarte());
        System.out.println("Carte tirée : " + talon.tirerCarte());

        // Afficher le nombre de cartes restantes
        talon.afficherNbCartesRestantes();
    }
}
