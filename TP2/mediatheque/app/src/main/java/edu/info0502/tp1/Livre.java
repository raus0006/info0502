package edu.info0502.tp1;

/**
 * Représente un livre dans une médiathèque.
 * Hérite de la classe {@link Media} et ajoute des informations spécifiques à un livre,
 * comme l'auteur et la référence (ISBN).
 */
public class Livre extends Media implements Cloneable{

    private String auteur;// L'auteur du livre
    private String reference;// La référence (ISBN) du livre

     /**
     * Constructeur par défaut.
     * Initialise un livre avec des valeurs par défaut.
     */
    public Livre(){
        super();
        auteur = "inconnue";
        reference = "ISBN0001151";
    }

    /**
     * Constructeur par copie.
     * Crée une nouvelle instance de Livre en copiant les attributs d'un autre livre.
     *
     * @param l Le livre à copier.
     */
    public Livre(Livre l){
        super(l);
        this.auteur = l.auteur;
        this.reference = l.reference;
    }

    /**
     * Constructeur avec paramètres.
     * Permet de définir les informations spécifiques d'un livre.
     *
     * @param t Le titre du livre.
     * @param c La cote du livre.
     * @param n La note attribuée au livre.
     * @param a L'auteur du livre.
     * @param r La référence (ISBN) du livre.
     */
    public Livre(String t, StringBuffer c, int n, String a, String r){
        super(t,c,n);
        auteur = a;
        reference = r;
    }

     /**
     * Retourne l'auteur du livre.
     *
     * @return Le nom de l'auteur.
     */
    public String getAuteur(){return auteur;}

     /**
     * Retourne la référence (ISBN) du livre.
     *
     * @return La référence du livre.
     */
    public String getReference(){return reference;}

    /**
     * Modifie la référence (ISBN) du livre.
     *
     * @param ref La nouvelle référence du livre.
     */
    public void setReference(String ref) {this.reference = ref;}

    /**
     * Crée une copie du Livre.
     *
     * @return Une nouvelle instance de Livre avec les mêmes attributs.
     * @throws CloneNotSupportedException Si la classe Livre ne peut pas être clonée.
     */
    @Override
    public Livre clone() throws CloneNotSupportedException{
        Livre clone = (Livre)super.clone();
        return clone;
    }

    /**
     * Retourne une représentation textuelle du livre.
     *
     * @return Une chaîne décrivant les informations du livre.
     */
    @Override
    public String toString() {
        return super.toString() + ", Auteur: " + auteur + ", Reference: " + reference;
    }

    /**
     * Méthode principale pour tester la classe Livre.
     *
     * @param args Arguments de la ligne de commande, qui ne sert pas dans notre test.
     */
    public static void main(String[] args) {
        Media.SetNom("Médiathèque Universelle");
        Livre livre = new Livre("Le livre de la jungle", new StringBuffer("DFTY4HG"), 5, "R. Kippling", "2081263246");
        System.out.println(livre);
    }

}